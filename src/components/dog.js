import Phaser from 'phaser'

export default class Dog extends Phaser.Physics.Arcade.Sprite {
  /**
   * @param {Phaser.Scene} scene
   * @param {number} x
   * @param {number} y
   * @param {string} texture
   */
  constructor(scene, x, y, texture) {
    super(scene, x, y, texture)
    scene.add.existing(this)
    scene.physics.add.existing(this)
    this.body.collideWorldBounds = true
  }

  /**
   * @param {Phaser.Scene} scene
   * **/
  update(scene) {
    const speed = 4
    const jumpPower = -200
    if (scene.cursors.up.isDown && !scene.dog.body.touching.none) {
      scene.dog.setVelocityY(jumpPower)
    }
    if (scene.cursors.left.isDown) {
      scene.dog.x -= speed
    }
    if (scene.cursors.right.isDown) {
      scene.dog.x += speed
    }
  }
}
