/**
 * @param {Phaser.Scene} scene
 * @param {string} groundTileTexture
 */
const createGround = (scene, groundTileTexture) => {
  const platforms = scene.physics.add.staticGroup()
  const gameWidth = scene.physics.world.bounds.width
  const groundTexture = scene.game.textures.get(groundTileTexture)
  const groundTileWidth = groundTexture.source[0].width
  const tilesToCoverGround = Math.ceil(gameWidth / groundTileWidth)

  for (let index = 0; index < tilesToCoverGround; index++) {
    platforms.create(
      index * groundTileWidth,
      scene.physics.world.bounds.bottom,
      groundTexture,
    )
  }
  return platforms
}

export default createGround
