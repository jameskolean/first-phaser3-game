import Phaser from 'phaser'
import MyScene from './scenes/my-scene'

const config = {
  type: Phaser.AUTO,
  width: 800,
  height: 600,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 100 },
      debug: true,
    },
  },
  scene: [MyScene],
}

export default new Phaser.Game(config)
