import Phaser from 'phaser'
import createGround from '../components/ground'
import Dog from '../components/dog'

export default class MyScene extends Phaser.Scene {
  constructor() {
    super('MyScene')
  }

  preload() {
    this.load.image('dog', 'assets/doggy/Tiles/dogBrown.png')
    this.load.image('ground', 'assets/doggy/Tiles/grassHalfCenter.png')

    this.cursors = this.input.keyboard.createCursorKeys()
  }

  create() {
    this.dog = new Dog(this, 100, 100, 'dog')

    const ground = createGround(this, 'ground')
    this.physics.add.collider(this.dog, ground)
  }

  update() {
    this.dog.update(this)
  }
}
